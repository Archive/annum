/*
 * Copyright (C) 2010, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Gustavo Noronha Silva
 */

#include "config.h"
#include "annum-shell-window.h"

#include "annum-shell-sidebar.h"
#include "annum-shell-view.h"
#include <glib/gi18n.h>
#include <calendar/gui/gnome-cal.h>
#include <shell/e-shell-window.h>

G_DEFINE_TYPE (AnnumShellWindow, annum_shell_window, E_TYPE_SHELL_WINDOW)

struct _AnnumShellWindowPrivate {
	AnnumShellView *shell_view;

	GtkWidget *vbox;
	GtkWidget *toolbox;
	GtkWidget *hpaned;

	GtkActionGroup *action_group;

	GtkWidget *today_button;
	GtkWidget *week_button;
	GtkWidget *month_button;

	GtkWidget *quickadd_entry;

	GtkWidget *quit_button;

	EShellSidebar *sidebar;
	EShellContent *content_view;
};

#define GET_PRIVATE(o)                                                  \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), ANNUM_TYPE_SHELL_WINDOW, AnnumShellWindowPrivate))

/* Action callbacks */
enum {
	ANNUM_VIEW_TODAY,
	ANNUM_VIEW_WEEK,
	ANNUM_VIEW_MONTH,
	ANNUM_VIEW_DAY,
	ANNUM_LAST_VIEW
};

static void annum_shell_window_change_view_cb (GtkAction *action, GtkRadioAction *current, AnnumShellWindow *self)
{
	AnnumShellWindowPrivate *priv = self->priv;
	const char *view_id = NULL;

	switch (gtk_radio_action_get_current_value (GTK_RADIO_ACTION (action))) {
	case ANNUM_VIEW_TODAY:
		annum_shell_view_goto_today (priv->shell_view);
		return;
	case ANNUM_VIEW_DAY:
		view_id = "Day_View";
		return;
	case ANNUM_VIEW_WEEK:
		view_id = "Week_View";
		break;
	case ANNUM_VIEW_MONTH:
		view_id = "Month_View";
		break;
	default:
		g_warning ("Unknown view asked for!");
		break;
	}

	if (view_id == NULL)
		return;

	/* See annum-shell-backend.c's class init for the view name */
	g_object_set (priv->shell_view, "view-id", view_id, NULL);
}

static void date_changed_cb (AnnumShellWindow *self,
			     GnomeCalendarViewType view,
			     AnnumShellView *shell_view)
{
	const char *view_id;
	ECalendar *date_navigator;
	GDate *today;
	GDate start;
	GDate end;

	if (view != GNOME_CAL_DAY_VIEW)
		return;

	/* Week_View also uses GNOME_CAL_DAY_VIEW, so make sure */
	view_id = e_shell_view_get_view_id (E_SHELL_VIEW (shell_view));
	if (!g_strcmp0 (view_id, "Week_View"))
		return;

	/* DAY_VIEW is treated specially, because we only want to have
	 * the button toggled if we are showing today, not any other
	 * day
	 */

	date_navigator = annum_shell_sidebar_get_date_navigator (ANNUM_SHELL_SIDEBAR (self->priv->sidebar));

	today = g_date_new ();
	g_date_set_time_t (today, time (NULL));

	e_calendar_item_get_selection (date_navigator->calitem,
				       &start, &end);

	if (g_date_compare (today, &start) || g_date_compare (&start, &end)) {
		GtkAction *action = gtk_action_group_get_action (self->priv->action_group,
								 "ShowDay");
		gtk_radio_action_set_current_value (GTK_RADIO_ACTION (action),
						    ANNUM_VIEW_DAY);
	}

	g_date_free (today);
}

static void close_window_cb (GtkButton *button, AnnumShellWindow *self)
{
	EShell *shell;

	shell = e_shell_window_get_shell (E_SHELL_WINDOW (self));
	e_shell_quit (shell);
}

/* Action definitions */
static GtkRadioActionEntry annum_view_actions[] = {
	{ "ShowToday", NULL, N_("Today"), "<Alt>1",
	  N_("Today's events"), ANNUM_VIEW_TODAY },

	{ "ShowWeek", NULL, N_("Week"), "<Alt>2",
	  N_("Week's events"), ANNUM_VIEW_WEEK },

	{ "ShowMonth", NULL, N_("Month"), "<Alt>3",
	  N_("Month's events"), ANNUM_VIEW_MONTH },

	{ "ShowDay", NULL, N_("Day"), NULL,
	  N_("Day's events"), ANNUM_VIEW_DAY },
};

static GObject *constructor (GType type,
			     guint n_properties,
			     GObjectConstructParam * properties)
{
	AnnumShellWindow *self;
	AnnumShellWindowPrivate *priv;
	GObject *object;
	GObjectClass *parent_class;
	GtkAction *action;
	GdkScreen *screen;
	gint monitor;
	GdkRectangle rect;
	GtkRadioAction *radio_action;
	GtkWidget *button_image;

	parent_class = G_OBJECT_CLASS (annum_shell_window_parent_class);
	object = parent_class->constructor (type, n_properties, properties);

	self = ANNUM_SHELL_WINDOW (object);
	priv = self->priv = GET_PRIVATE (self);

	gtk_widget_realize (GTK_WIDGET (self));

	screen = gdk_screen_get_default ();
	monitor =
	    gdk_screen_get_monitor_at_window (screen,
					      GTK_WIDGET (self)->window);
	gdk_screen_get_monitor_geometry (screen, monitor, &rect);
	gtk_window_set_default_size (GTK_WINDOW (self), rect.width,
				     rect.height);
	gtk_window_set_decorated (GTK_WINDOW (self), FALSE);

	/* This is a fake action, just because EShellView needs one, but we
	 * have only one view, so an action to switch views is needless.
	 */
	radio_action = gtk_radio_action_new ("Calendar", NULL, NULL, NULL, 0);

	priv->shell_view = g_object_new (ANNUM_TYPE_SHELL_VIEW,
					 "action", radio_action,
					 "shell-window", self, NULL);

	g_signal_connect_swapped (priv->shell_view, "date-changed",
				  G_CALLBACK (date_changed_cb),
				  self);

	/* Main layout */
	priv->vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (self), priv->vbox);

	priv->toolbox = gtk_hbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (priv->toolbox), 6);
	gtk_box_pack_start (GTK_BOX (priv->vbox), priv->toolbox,
			    FALSE, FALSE, 0);

	/* Top toolbox */
	priv->today_button = gtk_toggle_button_new ();
	gtk_box_pack_start (GTK_BOX (priv->toolbox), priv->today_button, FALSE, FALSE, 0);

	priv->week_button = gtk_toggle_button_new ();
	gtk_box_pack_start (GTK_BOX (priv->toolbox), priv->week_button, FALSE, FALSE, 0);

	priv->month_button = gtk_toggle_button_new ();
	gtk_box_pack_start (GTK_BOX (priv->toolbox), priv->month_button, FALSE, FALSE, 0);

	priv->quickadd_entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (priv->quickadd_entry), "This space intentionally left blank.");
	gtk_widget_set_sensitive (priv->quickadd_entry, FALSE);
	gtk_box_pack_start (GTK_BOX (priv->toolbox), priv->quickadd_entry, TRUE, TRUE, 0);

	priv->quit_button = gtk_button_new ();
	g_signal_connect (priv->quit_button, "clicked",
			  G_CALLBACK (close_window_cb), self);
	gtk_box_pack_start (GTK_BOX (priv->toolbox), priv->quit_button, FALSE, FALSE, 0);

	button_image = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_container_add (GTK_CONTAINER (priv->quit_button), button_image);

        /* Sidebar, and main content view */
	priv->hpaned = gtk_hpaned_new ();
	/* FIXME: store the position somehow? */
	gtk_paned_set_position (GTK_PANED (priv->hpaned), 250);
	gtk_box_pack_start (GTK_BOX (priv->vbox), priv->hpaned,
			    TRUE, TRUE, 0);

	priv->sidebar =
	    e_shell_view_get_shell_sidebar (E_SHELL_VIEW (priv->shell_view));
	gtk_paned_add1 (GTK_PANED (priv->hpaned), GTK_WIDGET (priv->sidebar));

	priv->content_view =
	    e_shell_view_get_shell_content (E_SHELL_VIEW (priv->shell_view));
	gtk_paned_add2 (GTK_PANED (priv->hpaned),
			GTK_WIDGET (priv->content_view));

	/* Set up actions */
	priv->action_group = gtk_action_group_new ("AnnumActions");
	gtk_action_group_add_radio_actions (priv->action_group,
					    annum_view_actions,
					    G_N_ELEMENTS (annum_view_actions),
					    ANNUM_VIEW_TODAY,
					    G_CALLBACK (annum_shell_window_change_view_cb),
					    self);

	/* Today */
	action = gtk_action_group_get_action (priv->action_group, "ShowToday");
	gtk_activatable_set_related_action (GTK_ACTIVATABLE (priv->today_button), action);

	/* Week */
	action = gtk_action_group_get_action (priv->action_group, "ShowWeek");
	gtk_activatable_set_related_action (GTK_ACTIVATABLE (priv->week_button), action);

	/* Month */
	action = gtk_action_group_get_action (priv->action_group, "ShowMonth");
	gtk_activatable_set_related_action (GTK_ACTIVATABLE (priv->month_button), action);

	return object;
}

static void constructed (GObject * object)
{
	AnnumShellWindow *self = ANNUM_SHELL_WINDOW (object);
	EShell *shell;

	g_object_get (self, "shell", &shell, NULL);

	if (shell)
		e_shell_watch_window (shell, GTK_WINDOW (self));
}

static void annum_shell_window_class_init (AnnumShellWindowClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	EShellWindowClass *shell_window_class = E_SHELL_WINDOW_CLASS (klass);

	gobject_class->constructor = constructor;
	gobject_class->constructed = constructed;

	shell_window_class->construct_toolbar = NULL;
	shell_window_class->construct_menubar = NULL;
	shell_window_class->create_shell_view = NULL;

	g_type_class_add_private (klass, sizeof (AnnumShellWindowPrivate));
}

static void annum_shell_window_init (AnnumShellWindow * self G_GNUC_UNUSED)
{
}

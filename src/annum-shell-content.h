/*
 * Based on e-cal-shell-content.h, from Evolution.
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 * Copyright (C) 2010 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 */

#ifndef __ANNUM_SHELL_CONTENT_H__
#define __ANNUM_SHELL_CONTENT_H__

#include "annum-shell-view.h"
#include <calendar/gui/gnome-cal.h>
#include <menus/gal-view-instance.h>
#include <shell/e-shell-content.h>
#include <shell/e-shell-searchbar.h>

/* Standard GObject macros */
#define ANNUM_TYPE_SHELL_CONTENT \
	(annum_shell_content_get_type ())
#define ANNUM_SHELL_CONTENT(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), ANNUM_TYPE_SHELL_CONTENT, AnnumShellContent))
#define ANNUM_SHELL_CONTENT_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), ANNUM_TYPE_SHELL_CONTENT, AnnumShellContentClass))
#define CALENDAR_IS_SHELL_CONTENT(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), ANNUM_TYPE_SHELL_CONTENT))
#define CALENDAR_IS_SHELL_CONTENT_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), ANNUM_TYPE_SHELL_CONTENT))
#define ANNUM_SHELL_CONTENT_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), ANNUM_TYPE_SHELL_CONTENT, AnnumShellContentClass))

G_BEGIN_DECLS typedef struct _AnnumShellContent AnnumShellContent;
typedef struct _AnnumShellContentClass AnnumShellContentClass;
typedef struct _AnnumShellContentPrivate AnnumShellContentPrivate;

enum {
	ANNUM_SHELL_CONTENT_SELECTION_SINGLE = 1 << 0,
	ANNUM_SHELL_CONTENT_SELECTION_MULTIPLE = 1 << 1,
	ANNUM_SHELL_CONTENT_SELECTION_IS_ASSIGNABLE = 1 << 2,
	ANNUM_SHELL_CONTENT_SELECTION_IS_COMPLETE = 1 << 3,
	ANNUM_SHELL_CONTENT_SELECTION_IS_EDITABLE = 1 << 4,
	ANNUM_SHELL_CONTENT_SELECTION_IS_MEETING = 1 << 5,
	ANNUM_SHELL_CONTENT_SELECTION_IS_ORGANIZER = 1 << 6,
	ANNUM_SHELL_CONTENT_SELECTION_IS_RECURRING = 1 << 7,
	ANNUM_SHELL_CONTENT_SELECTION_CAN_ACCEPT = 1 << 8,
	ANNUM_SHELL_CONTENT_SELECTION_CAN_DELEGATE = 1 << 9,
	ANNUM_SHELL_CONTENT_SELECTION_CAN_SAVE = 1 << 10
};

struct _AnnumShellContent {
	EShellContent parent;
	AnnumShellContentPrivate *priv;
};

struct _AnnumShellContentClass {
	EShellContentClass parent_class;
};

GType annum_shell_content_get_type (void);
GtkWidget *annum_shell_content_new (EShellView * shell_view);
ECalModel *annum_shell_content_get_model (AnnumShellContent *
					   cal_shell_content);
GnomeCalendar *annum_shell_content_get_calendar (AnnumShellContent *
						  cal_shell_content);
GalViewInstance *annum_shell_content_get_view_instance (AnnumShellContent *
							 cal_shell_content);

G_END_DECLS
#endif				/* ANNUM_SHELL_CONTENT_H */

/*
 * Based on e-cal-shell-content.c, from Evolution.
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 * Copyright (C) 2010 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 */

#include "config.h"
#include "annum-shell-content.h"

#include "annum-shell-view.h"
#include <calendar/gui/calendar-config.h>
#include <calendar/gui/calendar-view.h>
#include <calendar/gui/e-cal-list-view.h>
#include <calendar/gui/e-cal-model-calendar.h>
#include <calendar/gui/e-calendar-view.h>
#include <calendar/gui/e-day-view.h>
#include <calendar/gui/e-week-view.h>
#include <e-util/e-binding.h>
#include <e-util/gconf-bridge.h>
#include <glib/gi18n.h>
#include <menus/gal-view-etable.h>
#include <string.h>

G_DEFINE_TYPE (AnnumShellContent, annum_shell_content, E_TYPE_SHELL_CONTENT)
#define ANNUM_SHELL_CONTENT_GET_PRIVATE(obj)                         \
  (G_TYPE_INSTANCE_GET_PRIVATE                                          \
   ((obj), ANNUM_TYPE_SHELL_CONTENT, AnnumShellContentPrivate))
struct _AnnumShellContentPrivate {
	GtkWidget *notebook;

	GtkWidget *calendar;

	guint paned_binding_id;
};

enum {
	PROP_0,
	PROP_CALENDAR,
};

/* Used to indicate who has the focus within the calendar view. */
typedef enum {
	FOCUS_CALENDAR,
	FOCUS_OTHER
} FocusLocation;

static void
annum_shell_content_get_property (GObject * object,
				   guint property_id,
				   GValue * value, GParamSpec * pspec)
{
	switch (property_id) {
	case PROP_CALENDAR:
		g_value_set_object (value,
				    annum_shell_content_get_calendar
				    (ANNUM_SHELL_CONTENT (object)));
		return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void annum_shell_content_dispose (GObject * object)
{
	AnnumShellContentPrivate *priv;

	priv = ANNUM_SHELL_CONTENT_GET_PRIVATE (object);

	if (priv->notebook != NULL) {
		g_object_unref (priv->notebook);
		priv->notebook = NULL;
	}

	if (priv->calendar != NULL) {
		g_object_unref (priv->calendar);
		priv->calendar = NULL;
	}

	/* Chain up to parent's dispose() method. */
	G_OBJECT_CLASS (annum_shell_content_parent_class)->dispose (object);
}

static void annum_shell_content_finalize (GObject * object)
{
	AnnumShellContentPrivate *priv;

	priv = ANNUM_SHELL_CONTENT_GET_PRIVATE (object);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (annum_shell_content_parent_class)->finalize (object);
}

static void annum_shell_content_constructed (GObject * object)
{
	AnnumShellContentPrivate *priv;
	ECalendarView *calendar_view;
	EShell *shell;
	EShellContent *shell_content;
	EShellBackend *shell_backend;
	EShellSettings *shell_settings;
	EShellView *shell_view;
	EShellWindow *shell_window;
	GnomeCalendar *calendar;
	GtkWidget *container;
	GtkWidget *widget;
	const gchar *config_dir;
	gint ii;

	priv = ANNUM_SHELL_CONTENT_GET_PRIVATE (object);

	/* Chain up to parent's constructed() method. */
	G_OBJECT_CLASS (annum_shell_content_parent_class)->constructed
	    (object);

	shell_content = E_SHELL_CONTENT (object);
	shell_view = e_shell_content_get_shell_view (shell_content);
	shell_window = e_shell_view_get_shell_window (shell_view);

	shell_backend = e_shell_view_get_shell_backend (shell_view);
	config_dir = e_shell_backend_get_config_dir (shell_backend);

	shell = e_shell_window_get_shell (shell_window);
	shell_settings = e_shell_get_shell_settings (shell);

	/* Build content widgets. */
	widget = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (widget), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (widget), FALSE);
	gtk_container_add (GTK_CONTAINER (object), widget);
	priv->notebook = g_object_ref (widget);
	gtk_widget_show (widget);

	container = priv->notebook;

	/* Add views in the order defined by GnomeCalendarViewType, such
	 * that the notebook page number corresponds to the view type. */

	/* XXX GnomeCalendar is a widget, but we don't pack it.
	 *     Maybe it should just be a GObject instead? */
	priv->calendar = gnome_calendar_new (shell_settings);
	g_object_ref_sink (priv->calendar);
	calendar = GNOME_CALENDAR (priv->calendar);

	/* Initialize it to today */
	gnome_calendar_goto_today (calendar);

	/* FIXME: ListView is disabled for now - have to figure out
	 * the ETable integration before enabling it.
	 */
	for (ii = 0; ii < GNOME_CAL_LIST_VIEW; ii++) {
		calendar_view = gnome_calendar_get_calendar_view (calendar, ii);

		gtk_notebook_append_page (GTK_NOTEBOOK (container),
					  GTK_WIDGET (calendar_view), NULL);
		gtk_widget_show (GTK_WIDGET (calendar_view));
	}

	e_binding_new (priv->calendar, "view", priv->notebook, "page");

	container = widget;

}

static void annum_shell_content_class_init (AnnumShellContentClass* klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	EShellContentClass *e_shell_content_class = E_SHELL_CONTENT_CLASS (klass);

	object_class->get_property = annum_shell_content_get_property;
	object_class->dispose = annum_shell_content_dispose;
	object_class->finalize = annum_shell_content_finalize;
	object_class->constructed = annum_shell_content_constructed;

	/* Disable the searchbar */
	e_shell_content_class->construct_searchbar = NULL;

	g_object_class_install_property (object_class,
					 PROP_CALENDAR,
					 g_param_spec_object ("calendar",
							      NULL,
							      NULL,
							      GNOME_TYPE_CALENDAR,
							      G_PARAM_READABLE));

	g_type_class_add_private (klass, sizeof (AnnumShellContentPrivate));
}

static void
annum_shell_content_init (AnnumShellContent * annum_shell_content)
{
	annum_shell_content->priv =
	    ANNUM_SHELL_CONTENT_GET_PRIVATE (annum_shell_content);

	/* Postpone widget construction until we have a shell view. */
}

GtkWidget *annum_shell_content_new (EShellView * shell_view)
{
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	return g_object_new (ANNUM_TYPE_SHELL_CONTENT,
			     "shell-view", shell_view, NULL);
}

ECalModel *annum_shell_content_get_model (AnnumShellContent *
					   annum_shell_content)
{
	GnomeCalendar *calendar;

	g_return_val_if_fail (CALENDAR_IS_SHELL_CONTENT (annum_shell_content),
			      NULL);

	calendar = annum_shell_content_get_calendar (annum_shell_content);

	return gnome_calendar_get_model (calendar);
}

GnomeCalendar *annum_shell_content_get_calendar (AnnumShellContent *
						  annum_shell_content)
{
	g_return_val_if_fail (CALENDAR_IS_SHELL_CONTENT (annum_shell_content),
			      NULL);

	return GNOME_CALENDAR (annum_shell_content->priv->calendar);
}

/*
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
 */

#ifndef __ANNUM_SHELL_VIEW_H__
#define __ANNUM_SHELL_VIEW_H__

#include <shell/e-shell-view.h>

#define ANNUM_TYPE_SHELL_VIEW         (annum_shell_view_get_type ())
#define ANNUM_SHELL_VIEW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), ANNUM_TYPE_SHELL_VIEW, AnnumShellView))
#define ANNUM_SHELL_VIEW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), ANNUM_TYPE_SHELL_VIEW, AnnumShellViewClass))
#define ANNUM_IS_SHELL_VIEW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), ANNUM_TYPE_SHELL_VIEW))
#define ANNUM_IS_SHELL_VIEW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), ANNUM_TYPE_SHELL_VIEW))
#define ANNUM_SHELL_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), ANNUM_TYPE_SHELL_VIEW, AnnumShellViewClass))

typedef struct _AnnumShellViewPrivate AnnumShellViewPrivate;

typedef struct _AnnumShellView {
	EShellView parent;

	/* <private> */
	AnnumShellViewPrivate *priv;
} AnnumShellView;

typedef struct _AnnumShellViewClass {
	EShellViewClass parent_class;
} AnnumShellViewClass;

GType annum_shell_view_get_type (void);

GtkWidget *annum_shell_view_get_content_view (AnnumShellView * shell_view);

GtkWidget *annum_shell_view_get_sidebar (AnnumShellView * shell_view);

void annum_shell_view_goto_today (AnnumShellView *self);

#endif

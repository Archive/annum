/*
 * Based on e-cal-shell-sidebar.h, from Evolution.
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 * Copyright (C) 2010 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 */

#ifndef __ANNUM_SHELL_SIDEBAR_H__
#define __ANNUM_SHELL_SIDEBAR_H__

#include <libecal/e-cal.h>
#include <libedataserverui/e-source-selector.h>

#include <shell/e-shell-sidebar.h>
#include <shell/e-shell-view.h>
#include <misc/e-calendar.h>

/* Standard GObject macros */
#define ANNUM_TYPE_SHELL_SIDEBAR \
	(annum_shell_sidebar_get_type ())
#define ANNUM_SHELL_SIDEBAR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), ANNUM_TYPE_SHELL_SIDEBAR, AnnumShellSidebar))
#define ANNUM_SHELL_SIDEBAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), ANNUM_TYPE_SHELL_SIDEBAR, AnnumShellSidebarClass))
#define CALENDAR_IS_SHELL_SIDEBAR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), ANNUM_TYPE_SHELL_SIDEBAR))
#define CALENDAR_IS_SHELL_SIDEBAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), ANNUM_TYPE_SHELL_SIDEBAR))
#define ANNUM_SHELL_SIDEBAR_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), ANNUM_TYPE_SHELL_SIDEBAR, AnnumShellSidebarClass))

G_BEGIN_DECLS typedef struct _AnnumShellSidebar AnnumShellSidebar;
typedef struct _AnnumShellSidebarClass AnnumShellSidebarClass;
typedef struct _AnnumShellSidebarPrivate AnnumShellSidebarPrivate;

enum {
	ANNUM_SHELL_SIDEBAR_HAS_PRIMARY_SOURCE = 1 << 0,
	ANNUM_SHELL_SIDEBAR_CAN_DELETE_PRIMARY_SOURCE = 1 << 1,
	ANNUM_SHELL_SIDEBAR_PRIMARY_SOURCE_IS_SYSTEM = 1 << 2,
	ANNUM_SHELL_SIDEBAR_SOURCE_SUPPORTS_REFRESH = 1 << 3
};

struct _AnnumShellSidebar {
	EShellSidebar parent;
	AnnumShellSidebarPrivate *priv;
};

struct _AnnumShellSidebarClass {
	EShellSidebarClass parent_class;

	/* Signals */
	void (*client_added) (AnnumShellSidebar * cal_shell_sidebar,
			      ECal * client);
	void (*client_removed) (AnnumShellSidebar * cal_shell_sidebar,
				ECal * client);
	void (*status_message) (AnnumShellSidebar * cal_shell_sidebar,
				const gchar * status_message);
};

GType annum_shell_sidebar_get_type (void);
void annum_shell_sidebar_register_type (GTypeModule * type_module);
GtkWidget *annum_shell_sidebar_new (EShellView * shell_view);
GList *annum_shell_sidebar_get_clients (AnnumShellSidebar *
					 cal_shell_sidebar);
ECalendar *annum_shell_sidebar_get_date_navigator (AnnumShellSidebar *
						    cal_shell_sidebar);
ESourceSelector *annum_shell_sidebar_get_selector (AnnumShellSidebar *
						    cal_shell_sidebar);
void annum_shell_sidebar_add_source (AnnumShellSidebar * cal_shell_sidebar,
				      ESource * source);
void annum_shell_sidebar_remove_source (AnnumShellSidebar *
					 cal_shell_sidebar, ESource * source);

G_END_DECLS
#endif				/* ANNUM_SHELL_SIDEBAR_H */

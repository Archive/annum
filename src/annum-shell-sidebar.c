/*
 * Based on e-cal-shell-sidebar.c from Evolution.
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 * Copyright (C) 2010 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 */

#include "config.h"
#include "annum-shell-sidebar.h"

#include "annum-shell-backend.h"
#include "annum-shell-view.h"
#include <calendar/common/authentication.h>
#include <calendar/gui/calendar-config.h>
#include <calendar/gui/e-calendar-selector.h>
#include <calendar/gui/misc.h>
#include <e-util/e-alert-dialog.h>
#include <e-util/e-binding.h>
#include <e-util/gconf-bridge.h>
#include <glib/gi18n.h>
#include <libecal/e-cal.h>
#include <libedataserver/e-source-list.h>
#include <libedataserverui/e-source-selector.h>
#include <string.h>

G_DEFINE_TYPE (AnnumShellSidebar, annum_shell_sidebar, E_TYPE_SHELL_SIDEBAR)

struct _AnnumShellSidebarPrivate {
	GtkWidget *paned;
	GtkWidget *vbox;

	GtkWidget *selector;
	GtkWidget *add_calendar_button;

	GtkWidget *date_navigator;

	/* UID -> Client */
	GHashTable *client_table;
};

#define ANNUM_SHELL_SIDEBAR_GET_PRIVATE(obj)                         \
  (G_TYPE_INSTANCE_GET_PRIVATE                                          \
   ((obj), ANNUM_TYPE_SHELL_SIDEBAR, AnnumShellSidebarPrivate))

enum {
	PROP_0,
	PROP_DATE_NAVIGATOR,
	PROP_SELECTOR
};

enum {
	CLIENT_ADDED,
	CLIENT_REMOVED,
	STATUS_MESSAGE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void
annum_shell_sidebar_emit_client_added (AnnumShellSidebar *
					annum_shell_sidebar, ECal * client)
{
	guint signal_id = signals[CLIENT_ADDED];

	g_signal_emit (annum_shell_sidebar, signal_id, 0, client);
}

static void
annum_shell_sidebar_emit_client_removed (AnnumShellSidebar *
					  annum_shell_sidebar, ECal * client)
{
	guint signal_id = signals[CLIENT_REMOVED];

	g_signal_emit (annum_shell_sidebar, signal_id, 0, client);
}

static void
annum_shell_sidebar_emit_status_message (AnnumShellSidebar *
					  annum_shell_sidebar,
					  const gchar * status_message)
{
	guint signal_id = signals[STATUS_MESSAGE];

	g_signal_emit (annum_shell_sidebar, signal_id, 0, status_message);
}

static void
annum_shell_sidebar_backend_died_cb (AnnumShellSidebar *
				      annum_shell_sidebar, ECal * client)
{
	EShellView *shell_view;
	EShellWindow *shell_window;
	EShellSidebar *shell_sidebar;
	GHashTable *client_table;
	ESource *source;
	const gchar *uid;

	client_table = annum_shell_sidebar->priv->client_table;

	shell_sidebar = E_SHELL_SIDEBAR (annum_shell_sidebar);
	shell_view = e_shell_sidebar_get_shell_view (shell_sidebar);
	shell_window = e_shell_view_get_shell_window (shell_view);

	source = e_cal_get_source (client);
	uid = e_source_peek_uid (source);

	g_object_ref (source);

	g_hash_table_remove (client_table, uid);
	annum_shell_sidebar_emit_status_message (annum_shell_sidebar, NULL);

	e_alert_run_dialog_for_args (GTK_WINDOW (shell_window),
				     "calendar:calendar-crashed", NULL);

	g_object_unref (source);
}

static void
annum_shell_sidebar_backend_error_cb (AnnumShellSidebar *
				       annum_shell_sidebar,
				       const gchar * message, ECal * client)
{
	EShellView *shell_view;
	EShellWindow *shell_window;
	EShellSidebar *shell_sidebar;
	GtkWidget *dialog;
	const gchar *uri;
	gchar *uri_no_passwd;

	shell_sidebar = E_SHELL_SIDEBAR (annum_shell_sidebar);
	shell_view = e_shell_sidebar_get_shell_view (shell_sidebar);
	shell_window = e_shell_view_get_shell_window (shell_view);

	uri = e_cal_get_uri (client);
	uri_no_passwd = get_uri_without_password (uri);

	dialog = gtk_message_dialog_new (GTK_WINDOW (shell_window),
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
					 _("Error on %s\n%s"),
					 uri_no_passwd, message);

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	g_free (uri_no_passwd);
}

static void
annum_shell_sidebar_client_opened_cb (AnnumShellSidebar *
				       annum_shell_sidebar,
				       ECalendarStatus status, ECal * client)
{
	EShellView *shell_view;
	EShellWindow *shell_window;
	EShellSidebar *shell_sidebar;
	ESource *source;

	source = e_cal_get_source (client);

	shell_sidebar = E_SHELL_SIDEBAR (annum_shell_sidebar);
	shell_view = e_shell_sidebar_get_shell_view (shell_sidebar);
	shell_window = e_shell_view_get_shell_window (shell_view);

	if (status == E_CALENDAR_STATUS_AUTHENTICATION_FAILED ||
	    status == E_CALENDAR_STATUS_AUTHENTICATION_REQUIRED)
		e_auth_cal_forget_password (client);

	switch (status) {
	case E_CALENDAR_STATUS_OK:
		g_signal_handlers_disconnect_matched (client,
						      G_SIGNAL_MATCH_FUNC, 0, 0,
						      NULL,
						      annum_shell_sidebar_client_opened_cb,
						      NULL);

		annum_shell_sidebar_emit_status_message
		    (annum_shell_sidebar, _("Loading calendars"));
		annum_shell_sidebar_emit_client_added (annum_shell_sidebar,
							client);
		annum_shell_sidebar_emit_status_message
		    (annum_shell_sidebar, NULL);
		break;

	case E_CALENDAR_STATUS_AUTHENTICATION_FAILED:
		e_cal_open_async (client, FALSE);
		break;

	case E_CALENDAR_STATUS_BUSY:
		break;

	case E_CALENDAR_STATUS_REPOSITORY_OFFLINE:
		e_alert_run_dialog_for_args (GTK_WINDOW (shell_window),
					     "calendar:prompt-no-contents-offline-calendar",
					     NULL);
		break;

	default:
		annum_shell_sidebar_emit_client_removed
		    (annum_shell_sidebar, client);
		break;
	}
}

static void
annum_shell_sidebar_row_changed_cb (AnnumShellSidebar *
				     annum_shell_sidebar,
				     GtkTreePath * tree_path,
				     GtkTreeIter * tree_iter,
				     GtkTreeModel * tree_model)
{
	ESourceSelector *selector;
	ESource *source;

	/* XXX ESourceSelector's underlying tree store has only one
	 *     column: ESource objects.  While we're not supposed to
	 *     know this, listening for "row-changed" signals from
	 *     the model is easier to deal with than the selector's
	 *     "selection-changed" signal, which doesn't tell you
	 *     _which_ row changed. */

	selector = annum_shell_sidebar_get_selector (annum_shell_sidebar);
	gtk_tree_model_get (tree_model, tree_iter, 0, &source, -1);

	/* XXX This signal gets emitted a lot while the model is being
	 *     rebuilt, during which time we won't get a valid ESource.
	 *     ESourceSelector should probably block this signal while
	 *     rebuilding the model, but we'll be forgiving and not
	 *     emit a warning. */
	if (!E_IS_SOURCE (source))
		return;

	if (e_source_selector_source_is_selected (selector, source))
		annum_shell_sidebar_add_source (annum_shell_sidebar, source);
	else
		annum_shell_sidebar_remove_source (annum_shell_sidebar,
						    source);
}

static void
annum_shell_sidebar_selection_changed_cb (AnnumShellSidebar *
					   annum_shell_sidebar,
					   ESourceSelector * selector)
{
	GSList *list, *iter;

	/* This signal is emitted less frequently than "row-changed",
	 * especially when the model is being rebuilt.  So we'll take
	 * it easy on poor GConf. */

	list = e_source_selector_get_selection (selector);

	for (iter = list; iter != NULL; iter = iter->next) {
		ESource *source = iter->data;

		iter->data = (gpointer) e_source_peek_uid (source);
		g_object_unref (source);
	}

	calendar_config_set_calendars_selected (list);

	g_slist_free (list);
}

static void
annum_shell_sidebar_primary_selection_changed_cb (AnnumShellSidebar *
						   annum_shell_sidebar,
						   ESourceSelector * selector)
{
	EShell *shell;
	EShellView *shell_view;
	EShellWindow *shell_window;
	EShellSidebar *shell_sidebar;
	EShellSettings *shell_settings;
	ESource *source;

	/* XXX ESourceSelector needs a "primary-selection-uid" property
	 *     so we can just bind the property with GConfBridge. */

	source = e_source_selector_peek_primary_selection (selector);
	if (source == NULL)
		return;

	shell_sidebar = E_SHELL_SIDEBAR (annum_shell_sidebar);
	shell_view = e_shell_sidebar_get_shell_view (shell_sidebar);
	shell_window = e_shell_view_get_shell_window (shell_view);

	shell = e_shell_window_get_shell (shell_window);
	shell_settings = e_shell_get_shell_settings (shell);

	e_shell_settings_set_string (shell_settings, "cal-primary-calendar",
				     e_source_peek_uid (source));
}

static void
annum_shell_sidebar_get_property (GObject * object,
				   guint property_id,
				   GValue * value, GParamSpec * pspec)
{
	switch (property_id) {
	case PROP_DATE_NAVIGATOR:
		g_value_set_object (value,
				    annum_shell_sidebar_get_date_navigator
				    (ANNUM_SHELL_SIDEBAR (object)));
		return;

	case PROP_SELECTOR:
		g_value_set_object (value,
				    annum_shell_sidebar_get_selector
				    (ANNUM_SHELL_SIDEBAR (object)));
		return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void annum_shell_sidebar_dispose (GObject * object)
{
	AnnumShellSidebarPrivate *priv;

	priv = ANNUM_SHELL_SIDEBAR_GET_PRIVATE (object);

	if (priv->paned != NULL) {
		g_object_unref (priv->paned);
		priv->paned = NULL;
	}

	if (priv->vbox != NULL) {
		g_object_unref (priv->vbox);
		priv->vbox = NULL;
	}

	if (priv->selector != NULL) {
		g_object_unref (priv->selector);
		priv->selector = NULL;
	}

	if (priv->date_navigator != NULL) {
		g_object_unref (priv->date_navigator);
		priv->date_navigator = NULL;
	}

	g_hash_table_remove_all (priv->client_table);

	G_OBJECT_CLASS (annum_shell_sidebar_parent_class)->dispose (object);
}

static void annum_shell_sidebar_finalize (GObject * object)
{
	AnnumShellSidebarPrivate *priv;

	priv = ANNUM_SHELL_SIDEBAR_GET_PRIVATE (object);

	g_hash_table_destroy (priv->client_table);

	G_OBJECT_CLASS (annum_shell_sidebar_parent_class)->finalize (object);
}

static void annum_shell_sidebar_constructed (GObject * object)
{
	AnnumShellSidebarPrivate *priv;
	EShell *shell;
	EShellView *shell_view;
	EShellBackend *shell_backend;
	EShellSidebar *shell_sidebar;
	EShellSettings *shell_settings;
	ESourceSelector *selector;
	ESourceList *source_list;
	ESource *source;
	ECalendarItem *calitem;
	GtkWidget *widget;
	GConfBridge *bridge;
	GtkTreeModel *model;
	AtkObject *a11y;
	GSList *list, *iter;
	const gchar *key;
	gchar *uid;

	priv = ANNUM_SHELL_SIDEBAR_GET_PRIVATE (object);

	G_OBJECT_CLASS (annum_shell_sidebar_parent_class)->constructed
	    (object);

	shell_sidebar = E_SHELL_SIDEBAR (object);
	shell_view = e_shell_sidebar_get_shell_view (shell_sidebar);
	shell_backend = e_shell_view_get_shell_backend (shell_view);

	shell = e_shell_backend_get_shell (shell_backend);
	shell_settings = e_shell_get_shell_settings (shell);

	source_list =
	    annum_shell_backend_get_source_list (ANNUM_SHELL_BACKEND
						  (shell_backend));

	/* Paned - main divisor of the side bar */
	priv->paned = gtk_vpaned_new ();
	gtk_container_add (GTK_CONTAINER (shell_sidebar), priv->paned);
	g_object_ref (priv->paned);
	gtk_widget_show (priv->paned);

	/* VBox that separates the calendar chooser from the add button,
	 * first part of the paned.
	 */
	priv->vbox = gtk_vbox_new (FALSE, 0);
	gtk_paned_pack1 (GTK_PANED (priv->paned), priv->vbox, TRUE, TRUE);
	g_object_ref (priv->vbox);
	gtk_widget_show (priv->vbox);

	/* Scrolled window, with the calendar selector */
	widget = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (priv->vbox), widget);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (widget),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (widget),
					     GTK_SHADOW_IN);
	gtk_widget_show (widget);

	priv->selector = e_calendar_selector_new (source_list);
	e_source_selector_set_select_new (E_SOURCE_SELECTOR (priv->selector), TRUE);
	gtk_container_add (GTK_CONTAINER (widget), priv->selector);
	a11y = gtk_widget_get_accessible (priv->selector);
	atk_object_set_name (a11y, _("Calendar Selector"));
	priv->selector = g_object_ref (priv->selector);
	gtk_widget_show (priv->selector);

	/* Add calendar button */
	priv->add_calendar_button = gtk_button_new_with_label (_("Add calendar..."));
	gtk_box_pack_start (GTK_BOX (priv->vbox), priv->add_calendar_button,
			    FALSE, FALSE, 0);
	gtk_widget_show (priv->add_calendar_button);

	/* Mini-calendar (date navigator) - second part of the paned. */
	priv->date_navigator = e_calendar_new ();
	gtk_paned_pack2 (GTK_PANED (priv->paned), priv->date_navigator, FALSE, TRUE);
	calitem = E_CALENDAR (priv->date_navigator)->calitem;
	e_calendar_item_set_days_start_week_sel (calitem, 9);
	e_calendar_item_set_max_days_sel (calitem, 42);
	g_object_ref (priv->date_navigator);
	gtk_widget_show (priv->date_navigator);

	e_binding_new (shell_settings, "cal-show-week-numbers",
		       calitem, "show-week-numbers");

	e_binding_new (shell_settings, "cal-week-start-day",
		       calitem, "week-start-day");

	/* Restore the selector state from the last session. */

	selector = E_SOURCE_SELECTOR (priv->selector);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (selector));

	g_signal_connect_swapped (model, "row-changed",
				  G_CALLBACK
				  (annum_shell_sidebar_row_changed_cb),
				  object);

	source = NULL;
	uid =
	    e_shell_settings_get_string (shell_settings,
					 "cal-primary-calendar");
	if (uid != NULL)
		source = e_source_list_peek_source_by_uid (source_list, uid);
	if (source == NULL)
		source = e_source_list_peek_source_any (source_list);
	if (source != NULL)
		e_source_selector_set_primary_selection (selector, source);
	g_free (uid);

	list = calendar_config_get_calendars_selected ();
	for (iter = list; iter != NULL; iter = iter->next) {
		uid = iter->data;
		source = e_source_list_peek_source_by_uid (source_list, uid);
		g_free (uid);

		if (source == NULL)
			continue;

		e_source_selector_select_source (selector, source);
	}
	g_slist_free (list);

	/* Listen for subsequent changes to the selector. */

	g_signal_connect_swapped (selector, "selection-changed",
				  G_CALLBACK
				  (annum_shell_sidebar_selection_changed_cb),
				  object);

	g_signal_connect_swapped (selector, "primary-selection-changed",
				  G_CALLBACK
				  (annum_shell_sidebar_primary_selection_changed_cb),
				  object);

	/* Bind GObject properties to GConf keys. */
	bridge = gconf_bridge_get ();

	object = G_OBJECT (priv->paned);
	key = "/apps/evolution/calendar/display/date_navigator_vpane_position";
	gconf_bridge_bind_property_delayed (bridge, key, object, "position");
}

static guint32 annum_shell_sidebar_check_state (EShellSidebar * shell_sidebar)
{
	AnnumShellSidebar *annum_shell_sidebar;
	ESourceSelector *selector;
	ESource *source;
	gboolean can_delete = FALSE;
	gboolean is_system = FALSE;
	gboolean refresh_supported = FALSE;
	guint32 state = 0;

	annum_shell_sidebar = ANNUM_SHELL_SIDEBAR (shell_sidebar);
	selector = annum_shell_sidebar_get_selector (annum_shell_sidebar);
	source = e_source_selector_peek_primary_selection (selector);

	if (source != NULL) {
		ECal *client;
		const gchar *uri;
		const gchar *delete;

		uri = e_source_peek_relative_uri (source);
		is_system = (uri == NULL || strcmp (uri, "system") == 0);

		can_delete = !is_system;
		delete = e_source_get_property (source, "delete");
		can_delete &= (delete == NULL || strcmp (delete, "no") != 0);

		client =
		    g_hash_table_lookup (annum_shell_sidebar->priv->
					 client_table,
					 e_source_peek_uid (source));
		refresh_supported = client
		    && e_cal_get_refresh_supported (client);
	}

	if (source != NULL)
		state |= ANNUM_SHELL_SIDEBAR_HAS_PRIMARY_SOURCE;
	if (can_delete)
		state |= ANNUM_SHELL_SIDEBAR_CAN_DELETE_PRIMARY_SOURCE;
	if (is_system)
		state |= ANNUM_SHELL_SIDEBAR_PRIMARY_SOURCE_IS_SYSTEM;
	if (refresh_supported)
		state |= ANNUM_SHELL_SIDEBAR_SOURCE_SUPPORTS_REFRESH;

	return state;
}

static void
annum_shell_sidebar_client_removed (AnnumShellSidebar *
				     annum_shell_sidebar, ECal * client)
{
	ESourceSelector *selector;
	GHashTable *client_table;
	ESource *source;
	const gchar *uid;

	client_table = annum_shell_sidebar->priv->client_table;
	selector = annum_shell_sidebar_get_selector (annum_shell_sidebar);

	g_signal_handlers_disconnect_matched (client, G_SIGNAL_MATCH_DATA, 0, 0,
					      NULL, NULL, annum_shell_sidebar);

	source = e_cal_get_source (client);
	e_source_selector_unselect_source (selector, source);

	uid = e_source_peek_uid (source);
	g_hash_table_remove (client_table, uid);

	annum_shell_sidebar_emit_status_message (annum_shell_sidebar, NULL);
}

static void annum_shell_sidebar_class_init (AnnumShellSidebarClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	EShellSidebarClass *shell_sidebar_class = E_SHELL_SIDEBAR_CLASS (klass);

	gobject_class->get_property = annum_shell_sidebar_get_property;
	gobject_class->dispose = annum_shell_sidebar_dispose;
	gobject_class->finalize = annum_shell_sidebar_finalize;
	gobject_class->constructed = annum_shell_sidebar_constructed;

	shell_sidebar_class->check_state = annum_shell_sidebar_check_state;

	klass->client_removed = annum_shell_sidebar_client_removed;

	g_object_class_install_property (gobject_class,
					 PROP_DATE_NAVIGATOR,
					 g_param_spec_object ("date-navigator",
							      _
							      ("Date Navigator Widget"),
							      _
							      ("This widget displays a miniature calendar"),
							      E_TYPE_CALENDAR,
							      G_PARAM_READABLE));

	g_object_class_install_property (gobject_class,
					 PROP_SELECTOR,
					 g_param_spec_object ("selector",
							      _
							      ("Source Selector Widget"),
							      _
							      ("This widget displays groups of calendars"),
							      E_TYPE_SOURCE_SELECTOR,
							      G_PARAM_READABLE));

	signals[CLIENT_ADDED] = g_signal_new ("client-added",
					      G_OBJECT_CLASS_TYPE
					      (gobject_class),
					      G_SIGNAL_RUN_LAST,
					      G_STRUCT_OFFSET
					      (AnnumShellSidebarClass,
					       client_added), NULL, NULL,
					      g_cclosure_marshal_VOID__OBJECT,
					      G_TYPE_NONE, 1, E_TYPE_CAL);

	signals[CLIENT_REMOVED] = g_signal_new ("client-removed",
						G_OBJECT_CLASS_TYPE
						(gobject_class),
						G_SIGNAL_RUN_LAST,
						G_STRUCT_OFFSET
						(AnnumShellSidebarClass,
						 client_removed), NULL, NULL,
						g_cclosure_marshal_VOID__OBJECT,
						G_TYPE_NONE, 1, E_TYPE_CAL);

	signals[STATUS_MESSAGE] = g_signal_new ("status-message",
						G_OBJECT_CLASS_TYPE
						(gobject_class),
						G_SIGNAL_RUN_LAST |
						G_SIGNAL_ACTION,
						G_STRUCT_OFFSET
						(AnnumShellSidebarClass,
						 status_message), NULL, NULL,
						g_cclosure_marshal_VOID__STRING,
						G_TYPE_NONE, 1, G_TYPE_STRING);

	g_type_class_add_private (klass, sizeof (AnnumShellSidebarPrivate));
}

static void
annum_shell_sidebar_init (AnnumShellSidebar * annum_shell_sidebar)
{
	GHashTable *client_table;

	client_table = g_hash_table_new_full (g_str_hash, g_str_equal,
					      (GDestroyNotify) g_free,
					      (GDestroyNotify) g_object_unref);

	annum_shell_sidebar->priv =
	    ANNUM_SHELL_SIDEBAR_GET_PRIVATE (annum_shell_sidebar);

	annum_shell_sidebar->priv->client_table = client_table;
}

GtkWidget *annum_shell_sidebar_new (EShellView * shell_view)
{
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	return g_object_new (ANNUM_TYPE_SHELL_SIDEBAR,
			     "shell-view", shell_view, NULL);
}

GList *annum_shell_sidebar_get_clients (AnnumShellSidebar *
					 annum_shell_sidebar)
{
	GHashTable *client_table;

	g_return_val_if_fail (CALENDAR_IS_SHELL_SIDEBAR (annum_shell_sidebar),
			      NULL);

	client_table = annum_shell_sidebar->priv->client_table;

	return g_hash_table_get_values (client_table);
}

ECalendar *annum_shell_sidebar_get_date_navigator (AnnumShellSidebar *
						    annum_shell_sidebar)
{
	g_return_val_if_fail (CALENDAR_IS_SHELL_SIDEBAR (annum_shell_sidebar),
			      NULL);

	return E_CALENDAR (annum_shell_sidebar->priv->date_navigator);
}

ESourceSelector *annum_shell_sidebar_get_selector (AnnumShellSidebar *
						    annum_shell_sidebar)
{
	g_return_val_if_fail (CALENDAR_IS_SHELL_SIDEBAR (annum_shell_sidebar),
			      NULL);

	return E_SOURCE_SELECTOR (annum_shell_sidebar->priv->selector);
}

void
annum_shell_sidebar_add_source (AnnumShellSidebar * annum_shell_sidebar,
				 ESource * source)
{
	ESourceSelector *selector;
	GHashTable *client_table;
	ECal *client;
	const gchar *uid;
	const gchar *uri;
	gchar *message;

	g_return_if_fail (CALENDAR_IS_SHELL_SIDEBAR (annum_shell_sidebar));
	g_return_if_fail (E_IS_SOURCE (source));

	client_table = annum_shell_sidebar->priv->client_table;
	selector = annum_shell_sidebar_get_selector (annum_shell_sidebar);

	uid = e_source_peek_uid (source);
	client = g_hash_table_lookup (client_table, uid);

	if (client != NULL)
		return;

	client = e_auth_new_cal_from_source (source, E_CAL_SOURCE_TYPE_EVENT);
	g_return_if_fail (client != NULL);

	g_signal_connect_swapped (client, "backend-died",
				  G_CALLBACK
				  (annum_shell_sidebar_backend_died_cb),
				  annum_shell_sidebar);

	g_signal_connect_swapped (client, "backend-error",
				  G_CALLBACK
				  (annum_shell_sidebar_backend_error_cb),
				  annum_shell_sidebar);

	g_hash_table_insert (client_table, g_strdup (uid), client);
	e_source_selector_select_source (selector, source);

	uri = e_cal_get_uri (client);
	message = g_strdup_printf (_("Opening calendar at %s"), uri);
	annum_shell_sidebar_emit_status_message (annum_shell_sidebar,
						  message);
	g_free (message);

	g_signal_connect_swapped (client, "cal-opened",
				  G_CALLBACK
				  (annum_shell_sidebar_client_opened_cb),
				  annum_shell_sidebar);

	e_cal_open_async (client, FALSE);
}

void
annum_shell_sidebar_remove_source (AnnumShellSidebar *
				    annum_shell_sidebar, ESource * source)
{
	ESourceSelector *selector;
	GHashTable *client_table;
	ECal *client;
	const gchar *uid;

	g_return_if_fail (CALENDAR_IS_SHELL_SIDEBAR (annum_shell_sidebar));
	g_return_if_fail (E_IS_SOURCE (source));

	client_table = annum_shell_sidebar->priv->client_table;
	selector = annum_shell_sidebar_get_selector (annum_shell_sidebar);

	uid = e_source_peek_uid (source);
	client = g_hash_table_lookup (client_table, uid);

	if (client == NULL)
		return;

	annum_shell_sidebar_emit_client_removed (annum_shell_sidebar, client);
}

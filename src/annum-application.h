/*
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
 */

#ifndef __ANNUM_APPLICATION_H__
#define __ANNUM_APPLICATION_H__

#include <glib-object.h>

#define ANNUM_TYPE_APPLICATION         (annum_application_get_type ())
#define ANNUM_APPLICATION(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), ANNUM_TYPE_APPLICATION, AnnumApplication))
#define ANNUM_APPLICATION_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), ANNUM_TYPE_APPLICATION, AnnumApplicationClass))
#define ANNUM_IS_APPLICATION(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), ANNUM_TYPE_APPLICATION))
#define ANNUM_IS_APPLICATION_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), ANNUM_TYPE_APPLICATION))
#define ANNUM_APPLICATION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), ANNUM_TYPE_APPLICATION, AnnumApplicationClass))

typedef struct _AnnumApplicationPrivate AnnumApplicationPrivate;

typedef struct _AnnumApplication {
	GObject parent;

	/* <private> */
	AnnumApplicationPrivate *priv;
} AnnumApplication;

typedef struct _AnnumApplicationClass {
	GObjectClass parent_class;
} AnnumApplicationClass;

GType annum_application_get_type (void);

void annum_application_run (AnnumApplication * self);

#endif

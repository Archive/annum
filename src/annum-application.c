/*
 * Copyright (c) 2010, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
 */

#include "config.h"
#include "annum-application.h"

#include "annum-shell-backend.h"
#include "annum-shell-window.h"
#include <calendar/gui/e-timezone-entry.h>
#include <glib/gi18n.h>
#include <shell/e-shell.h>

G_DEFINE_TYPE (AnnumApplication, annum_application, G_TYPE_OBJECT)

struct _AnnumApplicationPrivate {
	EShell *shell;
};

#define GET_PRIVATE(o)                                                  \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), ANNUM_TYPE_APPLICATION, AnnumApplicationPrivate))

static GObject *constructor (GType type,
			     guint n_properties,
			     GObjectConstructParam * properties)
{
	AnnumApplication *self;
	AnnumApplicationPrivate *priv;
	GObjectClass *parent_class;
	GConfClient *gconf_client;
	gboolean start_offline = FALSE;
	const char *modules_directory;

	GError *error = NULL;

	parent_class = G_OBJECT_CLASS (annum_application_parent_class);
	self =
	    ANNUM_APPLICATION (parent_class->constructor
				(type, n_properties, properties));
	priv = self->priv = GET_PRIVATE (self);

	/* This is to allow running the program without installing. Notice
	 * that if you install, it will try to load modules from the
	 * installed directory instead.
	 */
	if (g_file_test (ANNUM_MODULES_DIRECTORY, G_FILE_TEST_IS_DIR))
		modules_directory = ANNUM_MODULES_DIRECTORY;
	else
		modules_directory = ".";

	/* Ensure the backend is already available in the type system, so
	 * that the shell sees it. This is needed because we are not a
	 * dynamically loadable module.
	 */
	g_type_name (ANNUM_TYPE_SHELL_BACKEND);

	priv->shell = g_object_new (E_TYPE_SHELL,
				    "name", "org.moblin.Calendar",
				    "module_directory", modules_directory,
				    NULL);
	gconf_client = e_shell_get_gconf_client (priv->shell);
	start_offline = gconf_client_get_bool (gconf_client,
					       "/apps/evolution/shell/start_offline",
					       &error);

	if (error) {
		g_warning ("Failed to read gconf key: %s", error->message);
		g_error_free (error);
		error = NULL;
	}

	g_object_set (priv->shell, "online", !start_offline, NULL);

	return G_OBJECT (self);
}

static void dispose (GObject * object)
{
	AnnumApplicationPrivate *priv = ANNUM_APPLICATION (object)->priv;

	if (priv->shell) {
		g_object_unref (priv->shell);
		priv->shell = NULL;
	}

	G_OBJECT_CLASS (annum_application_parent_class)->dispose (object);
}

static void annum_application_class_init (AnnumApplicationClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = dispose;
	gobject_class->constructor = constructor;

	g_type_class_add_private (klass, sizeof (AnnumApplicationPrivate));
}

static void annum_application_init (AnnumApplication * self G_GNUC_UNUSED)
{
}

/* API */
void annum_application_run (AnnumApplication * self)
{
	GtkWidget *main_window = g_object_new (ANNUM_TYPE_SHELL_WINDOW,
					       "shell", self->priv->shell,
					       NULL);
	gtk_widget_show_all (main_window);

	/* Hackish hack; this is so that the GTK+ UI builder finds the
	 * type, when creating dialos from .ui file definitions.
	 */
	g_type_name (E_TYPE_TIMEZONE_ENTRY);

	gtk_main ();
}

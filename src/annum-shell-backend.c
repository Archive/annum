/*
 * Based on e-cal-shell-backend.c from Evolution.
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 * Copyright (c) 2010, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"
#include "annum-shell-backend.h"

#include "annum-shell-settings.h"
#include "annum-shell-view.h"
#include <calendar/gui/calendar-config.h>
#include <e-util/e-import.h>
#include <glib/gi18n.h>
#include <libecal/e-cal.h>
#include <shell/e-shell-backend.h>

G_DEFINE_TYPE (AnnumShellBackend, annum_shell_backend, E_TYPE_SHELL_BACKEND)
#define ANNUM_SHELL_BACKEND_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), ANNUM_TYPE_SHELL_BACKEND, AnnumShellBackendPrivate))
#define CONTACTS_BASE_URI	"contacts://"
#define WEATHER_BASE_URI	"weather://"
#define WEB_BASE_URI		"webcal://"
#define PERSONAL_RELATIVE_URI	"system"
struct _AnnumShellBackendPrivate {
	ESourceList *source_list;
};

enum {
	PROP_0,
	PROP_SOURCE_LIST
};

static void
annum_shell_backend_get_property (GObject * object,
				   guint property_id,
				   GValue * value, GParamSpec * pspec)
{
	switch (property_id) {
	case PROP_SOURCE_LIST:
		g_value_set_object (value,
				    annum_shell_backend_get_source_list
				    (ANNUM_SHELL_BACKEND (object)));
		return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void annum_shell_backend_dispose (GObject * object)
{
	AnnumShellBackend *self = ANNUM_SHELL_BACKEND (object);
	AnnumShellBackendPrivate *priv = self->priv;

	if (priv->source_list != NULL) {
		g_object_unref (priv->source_list);
		priv->source_list = NULL;
	}

	/* Chain up to parent's dispose() method. */
	G_OBJECT_CLASS (annum_shell_backend_parent_class)->dispose (object);
}

static void annum_shell_backend_ensure_sources (EShellBackend * shell_backend)
{
	AnnumShellBackend *self = ANNUM_SHELL_BACKEND (shell_backend);
	AnnumShellBackendPrivate *priv = self->priv;
	ESourceGroup *on_this_computer;
	ESourceGroup *contacts;
	ESource *birthdays;
	ESource *personal;
	EShell *shell;
	EShellSettings *shell_settings;
	const gchar *data_dir, *name;
	gchar *base_uri, base_uri_seventh;
	gchar *filename;
	gchar *property;
	gboolean save_list = FALSE;

	birthdays = NULL;
	personal = NULL;

	shell = e_shell_backend_get_shell (shell_backend);
	shell_settings = e_shell_get_shell_settings (shell);

	if (!e_cal_get_sources
	    (&priv->source_list, E_CAL_SOURCE_TYPE_EVENT, NULL)) {
		g_warning ("Could not get calendar sources from GConf!");
		return;
	}

	data_dir = e_shell_backend_get_data_dir (shell_backend);
	filename = g_build_filename (data_dir, "local", NULL);
	base_uri = g_filename_to_uri (filename, NULL, NULL);
	g_free (filename);

	if (strlen (base_uri) > 7) {
		/* Compare only file:// part. If user home dir name
		 * changes we do not want to create one more group. */
		base_uri_seventh = base_uri[7];
		base_uri[7] = 0;
	} else {
		base_uri_seventh = -1;
	}

	on_this_computer = e_source_list_ensure_group (priv->source_list,
						       _("On This Computer"),
						       base_uri, TRUE);

	contacts = e_source_list_ensure_group (priv->source_list,
					       _("Contacts"),
					       CONTACTS_BASE_URI, TRUE);

	e_source_list_ensure_group (priv->source_list,
				    _("On The Web"), WEB_BASE_URI, FALSE);

	e_source_list_ensure_group (priv->source_list,
				    _("Weather"), WEATHER_BASE_URI, FALSE);

	if (base_uri_seventh != -1) {
		base_uri[7] = base_uri_seventh;
	}

	if (on_this_computer != NULL) {
		GSList *sources, *iter;
		const gchar *group_base_uri;

		sources = e_source_group_peek_sources (on_this_computer);
		group_base_uri =
		    e_source_group_peek_base_uri (on_this_computer);

		/* Make sure this group includes a "Personal" source. */
		for (iter = sources; iter != NULL; iter = iter->next) {
			ESource *source = iter->data;
			const gchar *relative_uri;

			relative_uri = e_source_peek_relative_uri (source);
			if (relative_uri == NULL)
				continue;

			if (strcmp (PERSONAL_RELATIVE_URI, relative_uri) != 0)
				continue;

			personal = source;
			break;
		}

		/* Make sure we have the correct base URI.  This can
		 * change when the user's home directory changes. */
		if (strcmp (base_uri, group_base_uri) != 0) {
			e_source_group_set_base_uri (on_this_computer,
						     base_uri);

			/* XXX We shouldn't need this sync call here as
			 *     set_base_uri() results in synching to GConf,
			 *     but that happens in an idle loop and too late
			 *     to prevent the user from seeing a "Cannot
			 *     Open ... because of invalid URI" error. */
			save_list = TRUE;
		}
	}

	name = _("Personal");

	if (personal == NULL) {
		ESource *source;
		GSList *selected;
		gchar *primary;

		source = e_source_new (name, PERSONAL_RELATIVE_URI);
		e_source_set_color_spec (source, "#BECEDD");
		e_source_group_add_source (on_this_computer, source, -1);
		g_object_unref (source);
		save_list = TRUE;

		primary =
		    e_shell_settings_get_string (shell_settings,
						 "cal-primary-calendar");

		selected = calendar_config_get_calendars_selected ();

		if (primary == NULL && selected == NULL) {
			const gchar *uid;

			uid = e_source_peek_uid (source);
			selected = g_slist_prepend (NULL, g_strdup (uid));

			e_shell_settings_set_string (shell_settings,
						     "cal-primary-calendar",
						     uid);

			calendar_config_set_calendars_selected (selected);
		}

		g_slist_foreach (selected, (GFunc) g_free, NULL);
		g_slist_free (selected);
		g_free (primary);
	} else {
		/* Force the source name to the current locale. */
		e_source_set_name (personal, name);
	}

	if (contacts != NULL) {
		GSList *sources;

		sources = e_source_group_peek_sources (contacts);

		if (sources != NULL) {
			GSList *trash;

			/* There is only one source under Contacts. */
			birthdays = E_SOURCE (sources->data);
			sources = g_slist_next (sources);

			/* Delete any other sources in this group.
			 * Earlier versions allowed you to create
			 * additional sources under Contacts. */
			trash = g_slist_copy (sources);
			while (trash != NULL) {
				ESource *source = trash->data;
				e_source_group_remove_source (contacts, source);
				trash = g_slist_delete_link (trash, trash);
				save_list = TRUE;
			}
		}
	}

	/* XXX e_source_group_get_property() returns a newly-allocated
	 *     string when it could just as easily return a const string.
	 *     Unfortunately, fixing that would break the API. */
	property = e_source_group_get_property (contacts, "create_source");
	if (property == NULL)
		e_source_group_set_property (contacts, "create_source", "no");
	g_free (property);

	name = _("Birthdays & Anniversaries");

	if (birthdays == NULL) {
		ESource *source;

		source = e_source_new (name, "/");
		e_source_group_add_source (contacts, source, -1);
		g_object_unref (source);
		save_list = TRUE;

		/* This is now a borrowed reference. */
		birthdays = source;
	} else {
		/* Force the source name to the current locale. */
		e_source_set_name (birthdays, name);
	}

	if (e_source_get_property (birthdays, "delete") == NULL)
		e_source_set_property (birthdays, "delete", "no");

	if (e_source_peek_color_spec (birthdays) == NULL)
		e_source_set_color_spec (birthdays, "#DDBECE");

	g_object_unref (on_this_computer);
	g_object_unref (contacts);
	g_free (base_uri);

	if (save_list)
		e_source_list_sync (priv->source_list, NULL);
}

static void annum_shell_backend_constructed (GObject * object)
{
	EShell *shell;
	EShellBackend *shell_backend;

	shell_backend = E_SHELL_BACKEND (object);
	shell = e_shell_backend_get_shell (shell_backend);

	annum_shell_backend_ensure_sources (shell_backend);

	annum_shell_backend_init_settings (shell);

	/* We do not initialize preferences here, because preferences are in
	 * a separate application
	 */
}

static void annum_shell_backend_class_init (AnnumShellBackendClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	EShellBackendClass *shell_backend_class = E_SHELL_BACKEND_CLASS (klass);

	gobject_class->constructed = annum_shell_backend_constructed;
	gobject_class->get_property = annum_shell_backend_get_property;
	gobject_class->dispose = annum_shell_backend_dispose;

	shell_backend_class->shell_view_type = ANNUM_TYPE_SHELL_VIEW;
	shell_backend_class->name = "Calendar";
	shell_backend_class->aliases = "";
	shell_backend_class->schemes = "calendar";
	shell_backend_class->sort_order = 400;	/* FIXME: Why? This comes from ECalShellBackend */
	shell_backend_class->preferences_page = NULL;
	shell_backend_class->start = NULL;
	shell_backend_class->migrate = NULL;

	g_type_class_add_private (klass, sizeof (AnnumShellBackendPrivate));
}

static void annum_shell_backend_init (AnnumShellBackend * self)
{
	self->priv = ANNUM_SHELL_BACKEND_GET_PRIVATE (self);
}

/* API */
ESourceList *annum_shell_backend_get_source_list (AnnumShellBackend *
						   shell_backend)
{
	g_return_val_if_fail (CALENDAR_IS_SHELL_BACKEND (shell_backend), NULL);

	return shell_backend->priv->source_list;
}

/*
 * Copyright (C) 2010, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Gustavo Noronha Silva
 */

#ifndef __ANNUM_SHELL_WINDOW_H__
#define __ANNUM_SHELL_WINDOW_H__

#include <shell/e-shell-window.h>

#define ANNUM_TYPE_SHELL_WINDOW         (annum_shell_window_get_type ())
#define ANNUM_SHELL_WINDOW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), ANNUM_TYPE_SHELL_WINDOW, AnnumShellWindow))
#define ANNUM_SHELL_WINDOW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), ANNUM_TYPE_SHELL_WINDOW, AnnumShellWindowClass))
#define ANNUM_IS_SHELL_WINDOW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), ANNUM_TYPE_SHELL_WINDOW))
#define ANNUM_IS_SHELL_WINDOW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), ANNUM_TYPE_SHELL_WINDOW))
#define ANNUM_SHELL_WINDOW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), ANNUM_TYPE_SHELL_WINDOW, AnnumShellWindowClass))

typedef struct _AnnumShellWindowPrivate AnnumShellWindowPrivate;

typedef struct _AnnumShellWindow {
	EShellWindow parent;

	/* private */
	AnnumShellWindowPrivate *priv;
} AnnumShellWindow;

typedef struct _AnnumShellWindowClass {
	EShellWindowClass parent_class;
} AnnumShellWindowClass;

GType annum_shell_window_get_type (void);

#endif

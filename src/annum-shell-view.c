/*
 * Copyright (c) 2010, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
 */

#include "config.h"
#include "annum-shell-view.h"

#include "annum-shell-backend.h"
#include "annum-shell-content.h"
#include "annum-shell-sidebar.h"
#include <calendar/gui/calendar-view-factory.h>
#include <calendar/gui/e-day-view.h>
#include <calendar/gui/e-week-view.h>
#include <libecal/e-cal-time-util.h>
#include <glib/gi18n.h>
#include <menus/gal-view-factory-etable.h>
#include <shell/e-shell-view.h>

G_DEFINE_TYPE (AnnumShellView, annum_shell_view, E_TYPE_SHELL_VIEW)

struct _AnnumShellViewPrivate {
	/* These are just for convenience. */
	AnnumShellBackend *prox_shell_backend;
	AnnumShellContent *prox_shell_content;
	AnnumShellSidebar *prox_shell_sidebar;

	/* The last time explicitly selected by the user. */
	time_t base_view_time;

	EActivity *calendar_activity;
};

#define GET_PRIVATE(o)                                                  \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), ANNUM_TYPE_SHELL_VIEW, AnnumShellViewPrivate))

enum {
	DATE_CHANGED,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

static void
annum_shell_view_date_navigator_selection_changed_cb (AnnumShellView * self,
						       ECalendarItem * calitem)
{
	AnnumShellContent *prox_shell_content;
	GnomeCalendarViewType switch_to;
	GnomeCalendarViewType view_type;
	GnomeCalendar *calendar;
	ECalModel *model;
	GDate start_date, end_date;
	GDate new_start_date, new_end_date;
	icaltimetype tt;
	icaltimezone *timezone;
	time_t start, end, new_time;
	gboolean starts_on_week_start_day;
	gint new_days_shown;
	gint week_start_day;

	prox_shell_content = self->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (prox_shell_content);

	model = gnome_calendar_get_model (calendar);
	view_type = gnome_calendar_get_view (calendar);
	switch_to = view_type;

	timezone = e_cal_model_get_timezone (model);
	week_start_day = e_cal_model_get_week_start_day (model);
	e_cal_model_get_time_range (model, &start, &end);

	time_to_gdate_with_zone (&start_date, start, timezone);
	time_to_gdate_with_zone (&end_date, end, timezone);

	if (view_type == GNOME_CAL_MONTH_VIEW) {
		EWeekView *week_view;
		ECalendarView *calendar_view;
		gboolean multi_week_view;
		gboolean compress_weekend;

		calendar_view =
		    gnome_calendar_get_calendar_view (calendar,
						      GNOME_CAL_MONTH_VIEW);

		week_view = E_WEEK_VIEW (calendar_view);
		multi_week_view = e_week_view_get_multi_week_view (week_view);
		compress_weekend = e_week_view_get_compress_weekend (week_view);

		if (week_start_day == 0
		    && (!multi_week_view || compress_weekend))
			g_date_add_days (&start_date, 1);
	}

	g_date_subtract_days (&end_date, 1);

	e_calendar_item_get_selection (calitem, &new_start_date, &new_end_date);

	/* There used to be a check here to make sure the rest of the
	 * code only ran when the date actually changed. We do not
	 * this to simplify always having three columns for the day
	 * view.
	 */

	new_days_shown =
	    g_date_get_julian (&new_end_date) -
	    g_date_get_julian (&new_start_date) + 1;

	/* If a complete week is selected we show the week view.
	 * Note that if weekends are compressed and the week start
	 * day is set to Sunday, we don't actually show complete
	 * weeks in the week view, so this may need tweaking. */
	starts_on_week_start_day =
	    (g_date_get_weekday (&new_start_date) % 7 == week_start_day);

	/* Update selection to be in the new time range. */
	tt = icaltime_null_time ();
	tt.year = g_date_get_year (&new_start_date);
	tt.month = g_date_get_month (&new_start_date);
	tt.day = g_date_get_day (&new_start_date);
	new_time = icaltime_as_timet_with_zone (tt, timezone);

	/* Switch views as appropriate, and change the number of
	 * days or weeks shown. */
	if (new_days_shown > 9) {
		if (view_type != GNOME_CAL_LIST_VIEW) {
			ECalendarView *calendar_view;

			calendar_view =
			    gnome_calendar_get_calendar_view (calendar,
							      GNOME_CAL_MONTH_VIEW);
			e_week_view_set_weeks_shown (E_WEEK_VIEW
						     (calendar_view),
						     (new_days_shown + 6) / 7);
			switch_to = GNOME_CAL_MONTH_VIEW;
		}
	} else if (new_days_shown == 7 && starts_on_week_start_day)
		switch_to = GNOME_CAL_WEEK_VIEW;
	else {
		ECalendarView *calendar_view;

		calendar_view =
		    gnome_calendar_get_calendar_view (calendar,
						      GNOME_CAL_DAY_VIEW);

		/* We always show three days */
		if (new_days_shown == 1)
		    new_days_shown = 3;

		e_day_view_set_days_shown (E_DAY_VIEW (calendar_view),
					   new_days_shown);

		if (new_days_shown != 5 || !starts_on_week_start_day)
			switch_to = GNOME_CAL_DAY_VIEW;

		else if (view_type != GNOME_CAL_WORK_WEEK_VIEW)
			switch_to = GNOME_CAL_DAY_VIEW;
	}

	/* Make the views display things properly. */
	gnome_calendar_update_view_times (calendar, new_time);
	gnome_calendar_set_view (calendar, switch_to);
	gnome_calendar_set_range_selected (calendar, TRUE);

	gnome_calendar_notify_dates_shown_changed (calendar);

	g_signal_emit (self, signals[DATE_CHANGED], 0, switch_to);
}

static struct tm
annum_shell_view_get_current_time (ECalendarItem * calitem,
				    AnnumShellView * prox_shell_view)
{
	AnnumShellContent *prox_shell_content;
	struct icaltimetype tt;
	icaltimezone *timezone;
	ECalModel *model;

	prox_shell_content = prox_shell_view->priv->prox_shell_content;
	model = annum_shell_content_get_model (prox_shell_content);
	timezone = e_cal_model_get_timezone (model);

	tt = icaltime_from_timet_with_zone (time (NULL), FALSE, timezone);

	return icaltimetype_to_tm (&tt);
}

static void
annum_shell_view_date_navigator_date_range_changed_cb (AnnumShellView *
							self,
							ECalendarItem * calitem)
{
	AnnumShellContent *shell_content;
	GnomeCalendar *calendar;

	shell_content = self->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (shell_content);

	gnome_calendar_update_query (calendar);
}

static void
annum_shell_view_selector_primary_changed_cb (AnnumShellView * self,
					       ESourceSelector * selector)
{
	AnnumShellContent *prox_shell_content;
	GnomeCalendar *calendar;
	ESource *source;

	/* XXX ESourceSelector -really- needs a "primary-selection"
	 *     ESource property.  Then we could just use EBindings. */

	prox_shell_content = self->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (prox_shell_content);
	source = e_source_selector_peek_primary_selection (selector);

	/* FIXME: removed in 7c02582ae70e45d58bf0966d60e23b50f854a06a */
	/* if (source != NULL) */
	/*	gnome_calendar_set_default_source (calendar, source); */
}

static void
annum_shell_view_selector_client_added_cb (AnnumShellView * shell_view,
					    ECal * client)
{
	AnnumShellContent *cal_shell_content;
	GnomeCalendar *calendar;
	ECalModel *model;

	cal_shell_content = shell_view->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (cal_shell_content);
	model = gnome_calendar_get_model (calendar);

	e_cal_model_add_client (model, client);
}

static void
annum_shell_view_selector_client_removed_cb (AnnumShellView * shell_view,
					      ECal * client)
{
	AnnumShellContent *cal_shell_content;
	GnomeCalendar *calendar;
	ECalModel *model;

	cal_shell_content = shell_view->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (cal_shell_content);
	model = gnome_calendar_get_model (calendar);

	e_cal_model_remove_client (model, client);
}

static void
annum_shell_view_user_created_cb (AnnumShellView * self,
				   ECalendarView * calendar_view)
{
	AnnumShellSidebar *annum_sidebar;
	ECalModel *model;
	ECal *client;
	ESource *source;

	model = e_calendar_view_get_model (calendar_view);
	client = e_cal_model_get_default_client (model);
	source = e_cal_get_source (client);

	annum_sidebar = self->priv->prox_shell_sidebar;
	annum_shell_sidebar_add_source (annum_sidebar, source);
}

void annum_shell_view_update_timezone (AnnumShellView * self)
{
	AnnumShellContent *annum_content;
	AnnumShellSidebar *annum_sidebar;
	icaltimezone *timezone;
	ECalModel *model;
	GList *clients, *iter;

	annum_content = self->priv->prox_shell_content;
	model = annum_shell_content_get_model (annum_content);
	timezone = e_cal_model_get_timezone (model);

	annum_sidebar = self->priv->prox_shell_sidebar;
	clients = annum_shell_sidebar_get_clients (annum_sidebar);

	for (iter = clients; iter != NULL; iter = iter->next) {
		ECal *client = iter->data;

		if (e_cal_get_load_state (client) == E_CAL_LOAD_LOADED)
			e_cal_set_default_timezone (client, timezone, NULL);
	}

	g_list_free (clients);
}

static GnomeCalendarViewType
gnome_cal_view_type_from_view_id (const char *view_id)
{
	if (g_str_equal (view_id, "Day_View"))
		return GNOME_CAL_DAY_VIEW;
	else if (g_str_equal (view_id, "Work_Week_View"))
		return GNOME_CAL_WORK_WEEK_VIEW;
	else if (g_str_equal (view_id, "Week_View"))
		return GNOME_CAL_WEEK_VIEW;
	else if (g_str_equal (view_id, "Month_View"))
		return GNOME_CAL_MONTH_VIEW;
	else if (g_str_equal (view_id, "List_View"))
		return GNOME_CAL_LIST_VIEW;
	else {
		g_warning ("Unknown view id: %s", view_id);
		return GNOME_CAL_LAST_VIEW;
	}
}

static void annum_shell_view_notify_view_id_cb (AnnumShellView * self)
{
	AnnumShellContent *shell_content;
	ECalendar *date_navigator;
	GnomeCalendar *calendar;
	const gchar *view_id;
	GnomeCalendarViewType view_type;
	GDate gdate_start;
	GDate gdate_end;

	view_id = e_shell_view_get_view_id (E_SHELL_VIEW (self));

	/* A NULL view ID implies we're in a custom view.  But you can
	 * only get to a custom view via the "Define Views" dialog, which
	 * would have already modified the view instance appropriately.
	 * Furthermore, there's no way to refer to a custom view by ID
	 * anyway, since custom views have no IDs. */
	if (view_id == NULL)
		return;

	view_type = gnome_cal_view_type_from_view_id (view_id);

	shell_content = self->priv->prox_shell_content;
	calendar = annum_shell_content_get_calendar (shell_content);

	gnome_calendar_display_view (calendar, view_type);

	if (view_type != GNOME_CAL_DAY_VIEW)
		return;

	/* Make sure just the day is selected, for this case */
	date_navigator = annum_shell_sidebar_get_date_navigator (self->priv->prox_shell_sidebar);
	e_calendar_item_get_selection (date_navigator->calitem,
				       &gdate_start, &gdate_end);
	e_calendar_item_set_selection (date_navigator->calitem,
				       &gdate_start, &gdate_start);
}

void constructed (GObject * object)
{
	AnnumShellView *self = ANNUM_SHELL_VIEW (object);
	AnnumShellViewPrivate *priv = self->priv;
	AnnumShellContent *prox_shell_content;
	AnnumShellSidebar *prox_shell_sidebar;
	EShellBackend *shell_backend;
	EShellContent *shell_content;
	EShellSidebar *shell_sidebar;
	EShellWindow *shell_window;
	EShellView *shell_view;
	GnomeCalendar *calendar;
	ECalendar *date_navigator;
	ESourceSelector *selector;
	ECalModel *model;
	int ii;

	G_OBJECT_CLASS (annum_shell_view_parent_class)->constructed (object);

	shell_view = E_SHELL_VIEW (self);
	shell_backend = e_shell_view_get_shell_backend (shell_view);
	shell_content = e_shell_view_get_shell_content (shell_view);
	shell_sidebar = e_shell_view_get_shell_sidebar (shell_view);
	shell_window = e_shell_view_get_shell_window (shell_view);

	e_shell_window_add_action_group (shell_window, "calendar");
	e_shell_window_add_action_group (shell_window, "calendar-filter");

	/* Cache these to avoid lots of awkward casting. */
	priv->prox_shell_backend = g_object_ref (shell_backend);
	priv->prox_shell_content = g_object_ref (shell_content);
	priv->prox_shell_sidebar = g_object_ref (shell_sidebar);

	prox_shell_content = ANNUM_SHELL_CONTENT (shell_content);
	model = annum_shell_content_get_model (prox_shell_content);
	calendar = annum_shell_content_get_calendar (prox_shell_content);

	prox_shell_sidebar = ANNUM_SHELL_SIDEBAR (shell_sidebar);
	selector = annum_shell_sidebar_get_selector (prox_shell_sidebar);
	date_navigator =
	    annum_shell_sidebar_get_date_navigator (prox_shell_sidebar);

	/* Give GnomeCalendar a handle to the date navigator. */
	gnome_calendar_set_date_navigator (calendar, date_navigator);

	e_calendar_item_set_get_time_callback (date_navigator->calitem,
					       (ECalendarItemGetTimeCallback)
					       annum_shell_view_get_current_time,
					       self, NULL);

	/* Initialize its default query to fetch everything in a range */
	gnome_calendar_set_search_query (calendar,
					 "(contains? \"summary\"  \"\")", FALSE,
					 0, 0);

	for (ii = 0; ii < GNOME_CAL_LAST_VIEW; ii++) {
		ECalendarView *calendar_view;

		calendar_view = gnome_calendar_get_calendar_view (calendar, ii);

		g_signal_connect_swapped (calendar_view, "user-created",
					  G_CALLBACK
					  (annum_shell_view_user_created_cb),
					  self);
	}

	g_signal_connect_swapped (date_navigator->calitem, "date-range-changed",
				  G_CALLBACK
				  (annum_shell_view_date_navigator_date_range_changed_cb),
				  self);

	g_signal_connect_swapped (date_navigator->calitem, "selection-changed",
				  G_CALLBACK
				  (annum_shell_view_date_navigator_selection_changed_cb),
				  self);

	g_signal_connect_swapped (selector, "primary-selection-changed",
				  G_CALLBACK
				  (annum_shell_view_selector_primary_changed_cb),
				  self);

	g_signal_connect_swapped (model, "notify::timezone",
				  G_CALLBACK
				  (annum_shell_view_update_timezone), self);

	g_signal_connect_swapped (prox_shell_sidebar, "client-added",
				  G_CALLBACK
				  (annum_shell_view_selector_client_added_cb),
				  self);

	g_signal_connect_swapped (prox_shell_sidebar, "client-removed",
				  G_CALLBACK
				  (annum_shell_view_selector_client_removed_cb),
				  self);

	/* Force the main calendar to update its default source. */
	g_signal_emit_by_name (selector, "primary-selection-changed");
}

static GObject *constructor (GType type,
			     guint n_properties,
			     GObjectConstructParam * properties)
{
	AnnumShellView *self;
	AnnumShellViewPrivate *priv;
	GObjectClass *parent_class;

	parent_class = G_OBJECT_CLASS (annum_shell_view_parent_class);
	self =
	    ANNUM_SHELL_VIEW (parent_class->constructor
			       (type, n_properties, properties));
	priv = self->priv = GET_PRIVATE (self);

	return G_OBJECT (self);
}

/* Might be a good idea to make EShellView's constructed only run the
 * class methods if they are not NULL
 */
static GtkWidget *create_place_holder_widget (EShellView * shell_view)
{
	return gtk_widget_new (GTK_TYPE_BUTTON, NULL);
}

static void annum_shell_view_class_init (AnnumShellViewClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	EShellViewClass *shell_view_class = E_SHELL_VIEW_CLASS (klass);

	gobject_class->constructor = constructor;
	gobject_class->constructed = constructed;

	shell_view_class->label = _("Calendar");
	shell_view_class->icon_name = "x-office-calendar";
	shell_view_class->ui_definition = NULL;
	shell_view_class->ui_manager_id = NULL;
	shell_view_class->search_options = NULL;
	shell_view_class->search_rules = "caltypes.xml";
	shell_view_class->new_shell_content = annum_shell_content_new;
	shell_view_class->new_shell_sidebar = annum_shell_sidebar_new;
	shell_view_class->new_shell_taskbar = create_place_holder_widget;
	shell_view_class->execute_search = NULL;
	shell_view_class->update_actions = NULL;

	signals[DATE_CHANGED] = g_signal_new ("date-changed",
					      ANNUM_TYPE_SHELL_VIEW,
					      G_SIGNAL_RUN_LAST,
					      0,
					      NULL,
					      NULL,
					      g_cclosure_marshal_VOID__INT, /* FIXME: ENUM */
					      G_TYPE_NONE,
					      1,
					      G_TYPE_INT);

	g_type_class_add_private (klass, sizeof (AnnumShellViewPrivate));
}

static void
annum_shell_view_load_view_collection (EShellViewClass * shell_view_class)
{
	GalViewCollection *collection;
	GalViewFactory *factory;

	collection = shell_view_class->view_collection;

	/* FIXME: check whether we want the ETable factory that used to
	 * exist here.
	 */

	factory = calendar_view_factory_new (GNOME_CAL_DAY_VIEW);
	gal_view_collection_add_factory (collection, factory);
	g_object_unref (factory);

	factory = calendar_view_factory_new (GNOME_CAL_WORK_WEEK_VIEW);
	gal_view_collection_add_factory (collection, factory);
	g_object_unref (factory);

	factory = calendar_view_factory_new (GNOME_CAL_WEEK_VIEW);
	gal_view_collection_add_factory (collection, factory);
	g_object_unref (factory);

	factory = calendar_view_factory_new (GNOME_CAL_MONTH_VIEW);
	gal_view_collection_add_factory (collection, factory);
	g_object_unref (factory);

	gal_view_collection_load (collection);
}

static void annum_shell_view_init (AnnumShellView * self G_GNUC_UNUSED)
{
	AnnumShellViewClass *klass = ANNUM_SHELL_VIEW_GET_CLASS (self);
	EShellViewClass *shell_view_class = E_SHELL_VIEW_CLASS (klass);

	if (!gal_view_collection_loaded (shell_view_class->view_collection))
		annum_shell_view_load_view_collection (shell_view_class);

	g_signal_connect (self, "notify::view-id",
			  G_CALLBACK (annum_shell_view_notify_view_id_cb),
			  NULL);

	/* Make sure we have an initial view set; this is very
	 * important, because the view, even if it is actually being
	 * displayed, will think it is not, and ignore all
	 * notifications coming from the model before it gets
	 * displayed via gnome_calendar_display_view, which ends up
	 * triggered by setting the view in the ShellView.
	 */
	e_shell_view_set_view_id (E_SHELL_VIEW (self), "Day_View");
}


/**
 * annum_shell_view_goto_today
 * @self: the #AnnumShellView
 *
 * This function handles the gory details of the special "today"
 * view. It makes sure only one day is selected in the date navigator,
 * and that said day is today.
 */
void annum_shell_view_goto_today (AnnumShellView *self)
{
	GDate *today;
	GnomeCalendar *calendar;
	ECalendar *date_navigator;

	today = g_date_new ();
	g_date_set_time_t (today, time NULL);

	calendar = annum_shell_content_get_calendar (self->priv->prox_shell_content);
	gnome_calendar_goto_today (calendar);

	date_navigator = annum_shell_sidebar_get_date_navigator (self->priv->prox_shell_sidebar);
	e_calendar_item_set_selection (date_navigator->calitem,
				       today, today);

	g_date_free (today);
}

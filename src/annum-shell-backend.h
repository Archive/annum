/*
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
 */

#ifndef __ANNUM_SHELL_BACKEND_H__
#define __ANNUM_SHELL_BACKEND_H__

#include <libedataserver/e-source-list.h>
#include <shell/e-shell-backend.h>

#define ANNUM_TYPE_SHELL_BACKEND         (annum_shell_backend_get_type ())
#define ANNUM_SHELL_BACKEND(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), ANNUM_TYPE_SHELL_BACKEND, AnnumShellBackend))
#define ANNUM_SHELL_BACKEND_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), ANNUM_TYPE_SHELL_BACKEND, AnnumShellBackendClass))
#define CALENDAR_IS_SHELL_BACKEND(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), ANNUM_TYPE_SHELL_BACKEND))
#define CALENDAR_IS_SHELL_BACKEND_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), ANNUM_TYPE_SHELL_BACKEND))
#define ANNUM_SHELL_BACKEND_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), ANNUM_TYPE_SHELL_BACKEND, AnnumShellBackendClass))

typedef struct _AnnumShellBackendPrivate AnnumShellBackendPrivate;

typedef struct _AnnumShellBackend {
	EShellBackend parent;

	/* private */
	AnnumShellBackendPrivate *priv;
} AnnumShellBackend;

typedef struct _AnnumShellBackendClass {
	EShellBackendClass parent_class;
} AnnumShellBackendClass;

GType annum_shell_backend_get_type (void);

ESourceList *annum_shell_backend_get_source_list (AnnumShellBackend *
						   shell_backend);

#endif
